﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Visualisation.Models
{
	public class CovidCsvModel
	{
		public DateTime Date_reported { get; set; }
		public string Country_code { get; set; }
		public string Country { get; set; }
		public string WHO_region { get; set; }
		public double New_cases { get; set; }
		public double Cumulative_cases { get; set; }
		public double New_deaths { get; set; }
		public double Cumulative_deaths { get; set; }
	}
}
