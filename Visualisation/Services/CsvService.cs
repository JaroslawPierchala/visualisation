﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CsvHelper;
using Visualisation.Abstractions;
using Visualisation.Models;

namespace Visualisation.Services
{
	public class CsvService : ICsvService
	{
		public List<CovidCsvModel> ReadCsv(string fullPath)
		{
			List<CovidCsvModel> recordsCsv = new List<CovidCsvModel>();
			using (var reader = new StreamReader(fullPath))
			using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
			{
				csv.Configuration.Delimiter = ",";
				csv.Configuration.MissingFieldFound = null;
				

				recordsCsv = csv.GetRecords<CovidCsvModel>().ToList();
			}
			return recordsCsv;
		}
	}
}
