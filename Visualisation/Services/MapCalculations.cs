﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Visualisation.Abstractions;
using Visualisation.Models;

namespace Visualisation.Services
{
	public class MapCalculations : IMapCalculations
	{
		public void CreateJSFileCovidCases(List<CovidCsvModel> covidData, string filePath)
		{
			string jsFile = "var covidData = {";
			double allCases = 0;
			foreach (var Country in covidData
				.OrderByDescending(x => x.Date_reported)
				.Where(x => x.Date_reported == covidData.OrderByDescending(x => x.Date_reported).Select(x => x.Date_reported).FirstOrDefault())
				.Select(x => x.Country).Take(235))
			{
				allCases += covidData.OrderByDescending(x => x.Date_reported).Where(x => x.Country == Country).FirstOrDefault().Cumulative_cases;
			}
			foreach (var CountryCode in covidData
				.OrderByDescending(x => x.Date_reported)
				.Where(x => x.Date_reported == covidData.OrderByDescending(x => x.Date_reported).Select(x => x.Date_reported).FirstOrDefault() && x.Country_code != " ")
				.Select(x => x.Country_code).Take(234))
			{
				jsFile += $"\n\"{CountryCode}\": {((covidData.OrderByDescending(x => x.Date_reported).Where(x => x.Country_code == CountryCode).FirstOrDefault().Cumulative_cases / allCases) * 255).ToString(CultureInfo.InvariantCulture)},";
			}
			jsFile = jsFile.Remove(jsFile.Length - 1, 1);
			jsFile += "};";
			File.WriteAllText(filePath, jsFile);
		}
	}
}
