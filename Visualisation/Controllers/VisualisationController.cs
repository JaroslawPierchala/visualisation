﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Visualisation.Abstractions;
using Visualisation.Models;

namespace Visualisation.Controllers
{
	public class VisualisationController : Controller
	{
		private IEnumerable<CovidCsvModel> CovidList;
		private readonly ICsvService _csvService;
		private readonly IWebHostEnvironment _environment;
		private readonly IMapCalculations _mapCalculations;
		private readonly ICharts _charts;
		public VisualisationController(IWebHostEnvironment environment, ICsvService csvService, ICharts charts, IMapCalculations mapCalculations)
		{
			_environment = environment;
			_csvService = csvService;
			_charts = charts;
			_mapCalculations = mapCalculations;
			string fullPath = Path.Combine(_environment.WebRootPath, "csv", "COVID-global-data.csv");
			CovidList = _csvService.ReadCsv(fullPath);
			//string filePath = Path.Combine(_environment.WebRootPath, "js", "mapData.js");
			//_mapCalculations.CreateJSFileCovidCases(CovidList.ToList(), filePath);
		}
		[HttpGet]
		public IActionResult Index()
		{
			List<CovidCsvModel> covidListCasesNow = new List<CovidCsvModel>();
			double allCases = 0;
			foreach (var CountryLastRecord in CovidList
				.OrderByDescending(x => x.Date_reported)
				.Where(x => x.Date_reported == CovidList.OrderByDescending(x => x.Date_reported).Select(x => x.Date_reported).FirstOrDefault())
				.Take(235))
			{
				covidListCasesNow.Add(CountryLastRecord);
				allCases += CovidList.OrderByDescending(x => x.Date_reported).Where(x => x.Country == CountryLastRecord.Country).FirstOrDefault().Cumulative_cases;
			}
			ViewData["WorldCases"] = allCases.ToString();
			return View(covidListCasesNow);
		}
        [HttpGet]
        public IActionResult PolishChart()
        {
			List<double?> covidData1 = new List<double?>();
            covidData1.AddRange(CovidList.OrderBy(x => x.Date_reported).Where(x => x.Country_code == "PL").Select(x => (double?)x.Cumulative_cases));
			List<double?> covidData2 = new List<double?>();
			covidData2.AddRange(CovidList.OrderBy(x => x.Date_reported).Where(x => x.Country_code == "PL").Select(x => (double?)x.Cumulative_deaths));
			List<string> Days = new List<string>();
            foreach (var Day in CovidList.OrderBy(x => x.Date_reported).Where(x => x.Country_code == "PL").Select(x => x.Date_reported.ToShortDateString()))
            {
                Days.Add(Day.ToString());
            }
            ViewData[$"PolishCases"] = _charts.getDoubleLineChart(covidData1, covidData2, Days, "Polish Cumulative Cases", "Polish Cumulative Deaths");

			return View();
        }
		public IActionResult EuropeChart()
		{
			List<double?> covidData1 = new List<double?>();
			foreach (var euCases in CovidList
				.OrderByDescending(x => x.Date_reported)
				.Where(x => x.WHO_region == "EURO" && x.Date_reported == CovidList.OrderByDescending(x => x.Date_reported).Select(x => x.Date_reported).FirstOrDefault())
				.Select(x => x.Cumulative_cases)
				.Take(53))
			{
				covidData1.Add(euCases);
			}
			List<double?> covidData2 = new List<double?>();
			foreach (var euDeaths in CovidList
				.OrderByDescending(x => x.Date_reported)
				.Where(x => x.WHO_region == "EURO" && x.Date_reported == CovidList.OrderByDescending(x => x.Date_reported).Select(x => x.Date_reported).FirstOrDefault())
				.Select(x => x.Cumulative_deaths)
				.Take(53))
			{
				covidData2.Add(euDeaths);
			}
			List<string> Countries = new List<string>();
			foreach (var country in CovidList
				.OrderByDescending(x => x.Date_reported)
				.Where(x => x.WHO_region == "EURO" && x.Date_reported == CovidList.OrderByDescending(x => x.Date_reported).Select(x => x.Date_reported).FirstOrDefault())
				.Select(x => x.Country)
				.Take(53))
			{
				Countries.Add(country.ToString());
			}
			ViewData[$"EuropeChart"] = _charts.getDoubleBarChart(covidData1, covidData2, Countries, "Europe Cumulative Cases", "Europe Cumulative Deaths");

			return View();
		}
    }
}
