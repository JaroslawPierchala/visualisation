﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Visualisation.Models;

namespace Visualisation.Abstractions
{
	public interface IMapCalculations
	{
		void CreateJSFileCovidCases(List<CovidCsvModel> covidData, string filePath);
	}
}
