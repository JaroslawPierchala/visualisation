﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Visualisation.Models;

namespace Visualisation.Abstractions
{
	public interface ICsvService
	{
		List<CovidCsvModel> ReadCsv(string fullPath);
	}
}
